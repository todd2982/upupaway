//Flying plugin for CraftBukkit.
//Copyright (C) 2012  Todd Stark
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

package biz.spcr.upupaway;

import java.util.logging.Logger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class upupaway extends JavaPlugin implements Listener {
    
    public static upupaway plugin;
    public final Logger logger = Logger.getLogger("Minecraft");
    public final ServerChatPlayerListener playerListener = new ServerChatPlayerListener(this);
    
    @Override
    public void onDisable() {
        // TODO: Place any custom disable code here.
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.info(pdfFile.getName() + " is now disabled.");
    }

    @Override
    public void onEnable() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvent(Event.Type.PLAYER_CHAT,this.playerListener, Event.Priority.Normal, this);        
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled.");
    } 
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args)
       {
           String commandName = command.getName().toLowerCase();
           String[] trimmedArgs = args;
           //sender.sendMessage(ChatColor.GREEN + trimmedArgs[0]);
           if(commandName.equals("upupaway"))
           {
               upupaway(sender, args);
           }
           return false;
       }
           public void upupaway(CommandSender sender, String[] args)
       {
           Player player = (Player)sender; // This is your player object
           //You can now use this to manipulate the player
           if (player.getAllowFlight() == false) {
           player.setAllowFlight(true);
           player.setFlying(true); } //example of manipulation 
           else if (player.getAllowFlight() == true) {
               player.setAllowFlight(false);
               player.setFlying(false);
           }
       }

    //@EventHandler
    //public void onPlayerJoin(PlayerJoinEvent event) {
    //    event.getPlayer().sendMessage("Welcome, " + event.getPlayer().getDisplayName() + "!");
    //}
}
